package wamz.the.unwantedroomsandroid.fcm;

import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import wamz.the.unwantedroomsandroid.MyRESTAPIURLs;
import wamz.the.unwantedroomsandroid.SharedPrefsFileName;

/**
 * Created by tinas on 02/06/2016.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("fcmToken", refreshedToken);
        editor.commit();

        // TODO: Implement this method to send any registration to your app's servers.
//        sendRegistrationToServer(refreshedToken, email);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p/>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token, String email) {
        try {
            OkHttpClient okHttpClient = new OkHttpClient();
            MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
            String postData = "gcm_registration_id=" + URLEncoder.encode(token, "UTF-8");
            RequestBody body = RequestBody.create(mediaType, postData);

            Request request = new Request.Builder()
                    .url(MyRESTAPIURLs.addGCMURL)
                    .header("Authorization", email)
                    .post(body)
                    .build();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful())
                        throw new IOException("Unexpected code " + response);

                    Log.i("Refresh Result", response.body().string());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseStoreFCMTokenInServerResponse(String response) {
        Toast.makeText(this, response, Toast.LENGTH_LONG).show();
        try {
            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.has("error") && !jsonObject.has("message"))
                return;

            if (jsonObject.getBoolean("error"))
                return;

            String message = jsonObject.getString("message");
            Log.i("Server Message", message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
