package wamz.the.unwantedroomsandroid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Profile;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import wamz.the.unwantedroomsandroid.IntentData;
import wamz.the.unwantedroomsandroid.JSONParser;
import wamz.the.unwantedroomsandroid.MyRESTAPIURLs;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.SharedPrefsFileName;
import wamz.the.unwantedroomsandroid.Utils;
import wamz.the.unwantedroomsandroid.asynctasks.DownloadImage;
import wamz.the.unwantedroomsandroid.models.Bid;
import wamz.the.unwantedroomsandroid.models.Hotel;
import wamz.the.unwantedroomsandroid.models.Images;
import wamz.the.unwantedroomsandroid.models.Room;

/**
 * Created by The on 13-Dec-15.
 */
public class RoomDetailsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private static final String TAG = "RoomDetailsActivity";
    private static final MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    private Toolbar toolbar;
    private Bundle bundle;
    private Room room;
    private ImageView roomImageView;
    private TextView roomDescriptionTextView,
            roomNameTextView,
            hotelNameTextView,
            roomSizeTextView,
            validForBidUntilTextView,
            bidStatusTextView,
            yourCurrentBidTextView,
            currencySign;
    private EditText enterYourBidEditText;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private OkHttpClient okHttpClient;
    private long days, hours, minutes, seconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_details);

        setUpToolBar();
        setUpUIElements();
        setUpNavigationDrawer();

        bundle = getIntent().getExtras();

        if (bundle.get("RoomID") != null) {
            int roomID = Integer.parseInt(bundle.get("RoomID").toString());
            Log.i("RoomID", bundle.get("RoomID").toString());
            getRoomFromServer(roomID);
            return;
        }
        else
            room = IntentData.getRoomFromIntent(bundle);

        displayRoomDetails(room);
        viewRoomImages();
        countdownTimer(room);
    }

    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setUpNavigationDrawer() {
        try {
            drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    // This method is used by the back button on the toolbar
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setUpUIElements() {
        roomImageView = (ImageView) findViewById(R.id.activity_room_details_room_image);
        roomDescriptionTextView = (TextView) findViewById(R.id.activity_room_details_room_description_textview);
        roomNameTextView = (TextView) findViewById(R.id.activity_room_details_room_name_textview);
        hotelNameTextView = (TextView) findViewById(R.id.activity_room_details_hotel_name_textview);
        roomSizeTextView = (TextView) findViewById(R.id.activity_room_details_room_size_textview);
        validForBidUntilTextView = (TextView) findViewById(R.id.activity_room_details_valid_for_bid_until_textview);
        bidStatusTextView = (TextView) findViewById(R.id.activity_room_details_bid_status_textview);
        yourCurrentBidTextView = (TextView) findViewById(R.id.activity_room_details_your_current_bid_textview);
        currencySign = (TextView) findViewById(R.id.activity_room_details_currency_sign_textview);
        enterYourBidEditText = (EditText) findViewById(R.id.activity_room_details_enter_your_bid_edittext);
    }

    private void displayRoomDetails(Room room) {
        new DownloadImage(roomImageView).execute(room.getImageURL());

        hotelNameTextView.setText(room.getHotel().getName());
        roomDescriptionTextView.setText(room.getRoomDescription());
        roomNameTextView.setText(room.getRoomName());
        roomSizeTextView.setText(room.getRoomSize());
        currencySign.setText(Utils.GetCorrectCurrency(room));
        bidStatusTextView.setText(getBidStatus() + Utils.GetCorrectCurrency(room) + Utils.TwoDecimalPlacesFormat(getBid()));
        yourCurrentBidTextView.setText("Your Current Bid: " + Utils.GetCorrectCurrency(room) + Utils.TwoDecimalPlacesFormat(getUserBidAmount()));

    }

    private String getBidStatus() {
        if (room.getLeadingBid() == 0)
            return "Starting Bid: ";
        else
            return "Leading Bid: ";
    }

    private double getBid() {
        if (room.getLeadingBid() == 0)
            return room.getStartingBidPrice();
        else
            return room.getLeadingBid();
    }

    private double getUserBidAmount() {
        if (room.getUserBid() != null)
            return room.getUserBid().getAmount();
        else
            return 0.00;
    }

    private void countdownTimer(Room room) {
        try {
            Calendar validForBidUntilDate = Calendar.getInstance();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.UK);
            validForBidUntilDate.setTime(simpleDateFormat.parse(room.getValidForBidUntil()));
            validForBidUntilDate.add(Calendar.MONTH, 1);
            Calendar today = Calendar.getInstance();
            today.add(Calendar.MONTH, 1);
            int secondsInADay = 24 * 60 * 60;
            long diff = validForBidUntilDate.getTimeInMillis() - today.getTimeInMillis();
            long diffSec = diff / 1000;
            days = diffSec / secondsInADay;
            long secondsDay = diffSec % secondsInADay;
            seconds = secondsDay % 60;
            minutes = (secondsDay / 60) % 60;
            hours = secondsDay / 3600;
            validForBidUntilTextView.setText("Time Left: \n" + days + Days(days) + hours + Hours(hours) + minutes + Minutes(minutes));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String Days(long days) {
        if (days == 1)
            return " Day ";
        else
            return " Days ";
    }

    private String Hours(long hours) {
        if (hours == 1)
            return " Hour ";
        else
            return " Hours ";
    }

    private String Minutes(long minutes) {
        if (minutes == 1)
            return " Minute ";
        else
            return " Minutes ";
    }

    private String Seconds(long seconds) {
        if (seconds == 1)
            return " Second ";
        else
            return " Seconds ";
    }

    private void viewRoomImages() {
        roomImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ViewRoomImagesActivity.class);
                intent.putExtra("Room", room);
                startActivity(intent);
            }
        });
    }

    private boolean BidEntered() {
        return !enterYourBidEditText.getText().toString().isEmpty();
    }

    private ArrayList<String> bidForRoomAsyncTaskParams() {
        ArrayList<String> params = new ArrayList<String>();
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
        String deviceKey = String.valueOf(sharedPreferences.getString("deviceKey", null));
        double enteredBid = 0;

        if (deviceKey == null)
            return null;

        if (!enterYourBidEditText.getText().toString().isEmpty())
            enteredBid = Double.parseDouble(enterYourBidEditText.getText().toString());

        params.add(deviceKey);
        params.add(String.valueOf(room.getId()));
        params.add(String.valueOf(Utils.TwoDecimalPlacesFormat(enteredBid)));

        return params;
    }

    private void placeBid(ArrayList<String> params) {
        try {
            String deviceKey = params.get(0);
            String roomID = params.get(1);
            String bidAmount = params.get(2);

            String postParams = "room_id=" + URLEncoder.encode(roomID, "UTF-8") + "&bid_amount=" + URLEncoder.encode(bidAmount, "UTF-8");

            RequestBody body = RequestBody.create(mediaType, postParams);
            Request request = new Request.Builder()
                    .url(MyRESTAPIURLs.bidsURL)
                    .addHeader("Authorization", deviceKey)
                    .post(body)
                    .build();

            okHttpClient = new OkHttpClient();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Unable to place bid! Please try again later", Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }

                    parsePlaceBidResponse();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parsePlaceBidResponse() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), "Your bid was successfully placed", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void PlaceBidButtonClicked(View view) {
        try {
            double enteredBid = 0;

            if (!BidEntered()) {
                Toast.makeText(this, "Please Enter Your Bid!", Toast.LENGTH_LONG).show();
                return;
            }

            if (!enterYourBidEditText.getText().toString().isEmpty())
                enteredBid = Double.parseDouble(enterYourBidEditText.getText().toString());

            if (enteredBid < room.getStartingBidPrice()) {
                Toast.makeText(this, "Your bid cannot be less than starting bid price!", Toast.LENGTH_LONG).show();
                return;
            }

            if (enteredBid < room.getLeadingBid()) {
                Toast.makeText(this, "Your bid cannot be less than the highest bid!", Toast.LENGTH_LONG).show();
                return;
            }

            placeBid(bidForRoomAsyncTaskParams());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getRoomFromServer(int roomID) {
        try {
            SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
            String deviceKey = String.valueOf(sharedPreferences.getString("deviceKey", null));

            Request request = new Request.Builder()
                    .url(MyRESTAPIURLs.roomsURL+roomID)
                    .addHeader("Authorization", deviceKey)
                    .build();

            okHttpClient = new OkHttpClient();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), "Room Not Found!", Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }

                    parseGetRoomFromServerResponse(response.body().string());
                }
            });
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseGetRoomFromServerResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject hotelJSONObject = jsonObject.getJSONObject("Hotel");
            JSONObject roomJSONObject = jsonObject.getJSONObject("Room");
            JSONArray imagesJSONArray = jsonObject.getJSONArray("Images");
            JSONArray bidsJSONArray = jsonObject.getJSONArray("Bid");

            Hotel hotel = JSONParser.parseHotel(hotelJSONObject);
            Images images = JSONParser.parseImages(JSONParser.parseImage(imagesJSONArray));
            ArrayList<Bid> listOfBids = JSONParser.parseBid(bidsJSONArray);
            room = JSONParser.parseRoom(roomJSONObject, hotel, images, listOfBids.get(0));

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    displayRoomDetails(room);
                    viewRoomImages();
                    countdownTimer(room);
                }
            });
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.nav_profile:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_dashboard:
                intent = new Intent(this, DashBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_bids:
                intent = new Intent(this, BidsActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
