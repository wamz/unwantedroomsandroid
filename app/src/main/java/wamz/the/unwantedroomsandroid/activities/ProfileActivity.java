package wamz.the.unwantedroomsandroid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import wamz.the.unwantedroomsandroid.IntentData;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.SharedPrefsFileName;

/**
 * Created by The on 20-Dec-15.
 */
public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private TextView nameTextView;
    private TextView emailTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initializing the facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_profile);

        SetUpToolBar();
        SetUpUIElements();
        setUpNavigationDrawer();
        DisplayProfile();
    }

    private void SetUpUIElements() {
        nameTextView = (TextView) findViewById(R.id.activity_profile_name_textview);
        emailTextView = (TextView) findViewById(R.id.activity_profile_email_textview);
    }

    private void SetUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void setUpNavigationDrawer() {
        drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void DisplayProfile() {
        SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPrefsFileName.tempFile, this.MODE_PRIVATE);
        String name = String.valueOf(sharedPreferences.getString("Name", null));
        String email = String.valueOf(sharedPreferences.getString("Email", null));

        if (name == null || email == null)
            return;

        nameTextView.setText(name);
        emailTextView.setText(email);
    }

    public void LogoutButtonClicked(View view) {
        new DeleteFirebaseInstanceAsyncTask().execute();
        LoginManager.getInstance().logOut();

        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
        sharedPreferences.edit().clear().commit();

        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.nav_profile:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_dashboard:
                intent = new Intent(this, DashBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_bids:
                intent = new Intent(this, BidsActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class DeleteFirebaseInstanceAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            try {
                FirebaseInstanceId.getInstance().deleteInstanceId();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
