package wamz.the.unwantedroomsandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import wamz.the.unwantedroomsandroid.IntentData;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.adapters.ViewRoomImagesAdapter;
import wamz.the.unwantedroomsandroid.models.Image;
import wamz.the.unwantedroomsandroid.models.Room;

/**
 * Created by The on 07-Dec-15.
 */
public class ViewRoomImagesActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ViewRoomImagesAdapter viewRoomImagesAdapter;
    private Bundle bundle;
    private Room room;
    private ArrayList<String> listOfURLs;
    private ListView viewRoomImagesListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_room_images);

        setUpToolBar();
        setUpUIElements();

        bundle = getIntent().getExtras();
        room = IntentData.getRoomFromIntent(bundle);
        listOfURLs = new ArrayList<String>();

        getImageURLsForRoom();
        displayListOfRoomImages();
        imageListItemClickListener();
    }

    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setUpUIElements(){
        viewRoomImagesListView = (ListView) findViewById(R.id.activity_view_room_images_listview);
    }

    private void getImageURLsForRoom(){
        for (Image image : room.getRoomImages().getListOfImages()) {
            if (image.getRoomID() != room.getId())
                continue;

            listOfURLs.add(image.getImageURL());
        }
    }

    // This method is used by the back button on the toolbar
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void displayListOfRoomImages() {
        // Instantiating a new SearchedRoomsAdapter to display list of room images
        viewRoomImagesAdapter = new ViewRoomImagesAdapter(this, R.layout.view_room_images_listitem, listOfURLs);
        // setting the adapter for the listview
        viewRoomImagesListView.setAdapter(viewRoomImagesAdapter);
    }

    /* This method holds the functionality for extracting the details of the room a user clicks
    * on from the list of rooms */
    private void imageListItemClickListener() {
        viewRoomImagesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String imageURL = (String) viewRoomImagesAdapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), RoomImageActivity.class);
                intent.putExtra("ImageURL", imageURL);
                startActivity(intent);
            }
        });
    }
}
