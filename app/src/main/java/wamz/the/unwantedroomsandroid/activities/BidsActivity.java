package wamz.the.unwantedroomsandroid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import wamz.the.unwantedroomsandroid.JSONParser;
import wamz.the.unwantedroomsandroid.MyRESTAPIURLs;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.SharedPrefsFileName;
import wamz.the.unwantedroomsandroid.adapters.BidsAdapter;
import wamz.the.unwantedroomsandroid.models.Bid;
import wamz.the.unwantedroomsandroid.models.Bids;
import wamz.the.unwantedroomsandroid.models.Hotel;
import wamz.the.unwantedroomsandroid.models.Hotels;
import wamz.the.unwantedroomsandroid.models.Image;
import wamz.the.unwantedroomsandroid.models.Images;
import wamz.the.unwantedroomsandroid.models.Room;
import wamz.the.unwantedroomsandroid.models.Rooms;

public class BidsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private LinearLayout linearLayout;
    private ListView bidsListView;
    private BidsAdapter searchedRoomsAdapter;
    private OkHttpClient okHttpClient;
    private Hotels hotels;
    private Rooms rooms;
    private Images images;
    private Bids bids;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bids);

        setUpToolBar();
        setUpUIElements();
        setUpNavigationDrawer();

        getBids();
        viewRoomListItemClickListener();
    }

    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setUpUIElements() {
        linearLayout = (LinearLayout) findViewById(R.id.activity_bids_linear_layout);
        bidsListView = (ListView) findViewById(R.id.activity_bids_list_of_bids_listview);
    }

    private void setUpNavigationDrawer() {
        try {
            drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void getBids() {
        try {
            SharedPreferences sharedPreferences = this.getSharedPreferences(
                    SharedPrefsFileName.tempFile, this.MODE_PRIVATE);

            String deviceKey = String.valueOf(sharedPreferences.getString("deviceKey", null));

            Request request = new Request.Builder()
                    .url(MyRESTAPIURLs.bidsURL)
                    .addHeader("Authorization", deviceKey)
                    .build();

            okHttpClient = new OkHttpClient();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Log.i("BidsActivity", "No Bids Returned!");
                            }
                        });
                        return;
                    }

                    parseGetBidsResponse(response.body().string());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseGetBidsResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray hotelsJSONArray = jsonObject.getJSONArray("Hotels");
            JSONArray roomsJSONArray = jsonObject.getJSONArray("Rooms");
            JSONArray imagesJSONArray = jsonObject.getJSONArray("Images");
            JSONArray bidsJSONArray = jsonObject.getJSONArray("Bids");

            ArrayList<Hotel> listOfHotels = JSONParser.parseHotel(hotelsJSONArray);
            hotels = JSONParser.parseHotels(listOfHotels);
            ArrayList<Image> listOfImages = JSONParser.parseImage(imagesJSONArray);
            images = JSONParser.parseImages(listOfImages);
            ArrayList<Bid> listOfBids = JSONParser.parseBid(bidsJSONArray);
            bids = JSONParser.parseBids(listOfBids);
            ArrayList<Room> listOfRooms = JSONParser.parseRoom(roomsJSONArray, hotels, images, bids);
            rooms = JSONParser.parseRooms(listOfRooms);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    displayListOfBids();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void displayListOfBids() {
        // Instantiating a new SearchedRoomsAdapter to display list of rooms
        searchedRoomsAdapter = new BidsAdapter(this, R.layout.listview_listitem, rooms.getListOfRooms());
        // setting the adapter for the listview
        bidsListView.setAdapter(searchedRoomsAdapter);
    }

    /* This method holds the functionality for extracting the details of the room a user clicks
    * on from the list of rooms */
    private void viewRoomListItemClickListener() {
        bidsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Room room = (Room) searchedRoomsAdapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), RoomDetailsActivity.class);
                intent.putExtra("Room", room);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.nav_profile:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_dashboard:
                intent = new Intent(this, DashBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_bids:
                intent = new Intent(this, BidsActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
