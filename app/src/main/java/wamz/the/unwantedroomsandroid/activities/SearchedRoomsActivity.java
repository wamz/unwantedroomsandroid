package wamz.the.unwantedroomsandroid.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.facebook.Profile;

import wamz.the.unwantedroomsandroid.IntentData;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.adapters.SearchedRoomsAdapter;
import wamz.the.unwantedroomsandroid.models.Room;
import wamz.the.unwantedroomsandroid.models.Rooms;

/**
 * Created by The on 20-Jan-16.
 */
public class SearchedRoomsActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private ListView searchedRoomsListView;
    private SearchedRoomsAdapter searchedRoomsAdapter;
    private Bundle bundle;
    private Rooms rooms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searched_rooms);

        setUpToolBar();
        setUpUIElements();
        setUpNavigationDrawer();

        bundle = getIntent().getExtras();
        rooms = IntentData.getRoomsFromIntent(bundle);
        displayListOfRooms();
        viewRoomListItemClickListener();
    }

    // This method is used by the back button on the toolbar
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void setUpUIElements() {
        searchedRoomsListView = (ListView) findViewById(R.id.activity_searched_rooms_listview);
    }

    private void setUpNavigationDrawer() {
        try {
            drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        }
        catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    private void displayListOfRooms() {
        // Instantiating a new SearchedRoomsAdapter to display list of rooms
        searchedRoomsAdapter = new SearchedRoomsAdapter(this, R.layout.listview_listitem, rooms.getListOfRooms());
        // setting the adapter for the listview
        searchedRoomsListView.setAdapter(searchedRoomsAdapter);
    }

    /* This method holds the functionality for extracting the details of the room a user clicks
    * on from the list of rooms */
    private void viewRoomListItemClickListener() {
        searchedRoomsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Room room = (Room) searchedRoomsAdapter.getItem(position);

                Intent intent = new Intent(getApplicationContext(), RoomDetailsActivity.class);
                intent.putExtra("Room", room);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.nav_profile:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_dashboard:
                intent = new Intent(this, DashBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_bids:
                intent = new Intent(this, BidsActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
