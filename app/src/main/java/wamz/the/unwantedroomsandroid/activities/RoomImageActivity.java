package wamz.the.unwantedroomsandroid.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;

import wamz.the.unwantedroomsandroid.IntentData;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.asynctasks.DownloadImage;

/**
 * Created by The on 13-Dec-15.
 */
public class RoomImageActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private Bundle bundle;
    private String imageURL;;
    private ImageView roomImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_image);

        SetUpToolBar();

        bundle = getIntent().getExtras();
        imageURL = IntentData.getImageURL(bundle);

        DisplayRoomImage();
    }

    private void SetUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    // This method is used by the back button on the toolbar
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void DisplayRoomImage() {
        roomImageView = (ImageView) findViewById(R.id.activity_room_image_imageview);
        new DownloadImage(roomImageView).execute(imageURL);
    }
}
