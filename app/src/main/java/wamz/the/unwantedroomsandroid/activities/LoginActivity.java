package wamz.the.unwantedroomsandroid.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import wamz.the.unwantedroomsandroid.HttpConnection;
import wamz.the.unwantedroomsandroid.MyRESTAPIURLs;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.SharedPrefsFileName;
import wamz.the.unwantedroomsandroid.baseclasses.BaseAsyncTask;

public class LoginActivity extends AppCompatActivity {
    private static final MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    private Toolbar toolbar;
    private LoginButton facebookLoginButton;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private SharedPreferences sharedPreferences;
    private Profile profile;
    private AccessToken accessToken;
    private String fcmToken;
    private ArrayList<String> fbLoginParams;
    private ArrayList<String> fbRegisterParams;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // initializing the facebook sdk
        FacebookSdk.sdkInitialize(getApplicationContext());
        // callback manager to handle login responses
        callbackManager = CallbackManager.Factory.create();
        fcmToken = FirebaseInstanceId.getInstance().getToken();

        initializeAccessTokenTracker();
        initializeProfileTracker();
        startTrackingAccessTokenAndProfile(accessTokenTracker, profileTracker);

        setContentView(R.layout.activity_login);

        setUpToolBar();

        facebookLoginButton = (LoginButton) findViewById(R.id.activity_login_facebook_login_button);

        setFacebookReadPermissions();
        initializeFacebookCallback();
    }

    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setFacebookReadPermissions() {
        List<String> permissionList = new ArrayList<String>();
        permissionList.add("public_profile");
        permissionList.add("email");

        facebookLoginButton.setReadPermissions(permissionList);
    }

    private void initializeFacebookCallback() {
        try {
            FacebookCallback<LoginResult> facebookCallback = new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    try {
                        accessToken = loginResult.getAccessToken();
                        profile = Profile.getCurrentProfile();
                        getUserFacebookData(accessToken);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onCancel() {
                    Toast.makeText(getApplicationContext(), "Unable to login! Please try again later", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    error.printStackTrace();
                }
            };

            facebookLoginButton.registerCallback(callbackManager, facebookCallback);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getUserFacebookData(AccessToken accessToken) {
        try {
            GraphRequest request = GraphRequest.newMeRequest(
                    accessToken,
                    new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                        public void onCompleted(JSONObject object, GraphResponse response) {
                            parseGetUserFacebookDataResponse(object);
                        }
                    });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "email, name");
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void parseGetUserFacebookDataResponse(JSONObject jsonObject) {
        try {
            String email = jsonObject.getString("email");
            Log.i("FB Email", email);

            sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Email", email);
            editor.commit();

            fbLoginParams = new ArrayList<String>();
            fbLoginParams.add(email);
            fbLoginParams.add(fcmToken);

            fbRegisterParams = new ArrayList<String>();
            fbRegisterParams.add(jsonObject.getString("name"));
            fbRegisterParams.add(jsonObject.getString("email"));
            fbRegisterParams.add("facebook");
            fbRegisterParams.add(fcmToken);

            new FacebookAuthLoginAsyncTask().execute(fbLoginParams);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initializeAccessTokenTracker() {
        try {
            accessTokenTracker = new AccessTokenTracker() {
                @Override
                protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                    accessToken = currentAccessToken;
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeProfileTracker() {
        try {
            profileTracker = new ProfileTracker() {
                @Override
                protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                    profile = currentProfile;
                    sendUserProfile(profile);
                }
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startTrackingAccessTokenAndProfile(AccessTokenTracker accessTokenTracker, ProfileTracker profileTracker) {
        try {
            accessTokenTracker.startTracking();
            profileTracker.startTracking();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTrackingAccessTokenAndProfile(AccessTokenTracker accessTokenTracker, ProfileTracker profileTracker) {
        try {
            accessTokenTracker.stopTracking();
            profileTracker.stopTracking();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendUserProfile(Profile profile) {
        try {
            if (profile == null)
                return;

            Log.i("profileName", profile.getName());
            sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("Name", profile.getName());
            editor.commit();

            Intent intent = new Intent(getApplicationContext(), DashBoardActivity.class);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        sendUserProfile(profile);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTrackingAccessTokenAndProfile(accessTokenTracker, profileTracker);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // forward the login results to the callbackManager created in onCreate method
        /* Every activity and fragment that integrates with the FacebookSDK Login or Share
           should forward onActivityResult to the callbackManager*/
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void handleAuthResponse(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);

            if (!jsonObject.has("Message") || !jsonObject.has("UserDetails"))
                return;

            String message = jsonObject.getString("Message");
            Log.i("FB Login Message", message);

            JSONObject userDetailsJSONObject = jsonObject.getJSONObject("UserDetails");

            SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString("deviceKey", userDetailsJSONObject.getString("device_id"));
            editor.commit();

            sendUserProfile(profile);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class FacebookAuthLoginAsyncTask extends BaseAsyncTask {

        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                response = performTask(params);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                new FacebookAuthRegisterAsyncTask().execute(fbRegisterParams);
                return;
            }

            handleAuthResponse(response);
        }

        protected String performTask(ArrayList<String>... params) {
            String response = null;
            try {
                // Setting the http connection to the server URL
                URL url = new URL(MyRESTAPIURLs.facebookAuthLoginURL);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // data to be sent to the REST-API
                String email = params[0].get(0);
                String fcmToken = params[0].get(1);

                String postData = "email=" + URLEncoder.encode(email, "UTF-8") +
                        "&gcm_registration_id=" + URLEncoder.encode(fcmToken, "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postData.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postData);
                // Checking if response code from REST-API is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }

    private class FacebookAuthRegisterAsyncTask extends BaseAsyncTask {
        @Override
        protected String doInBackground(ArrayList<String>... params) {
            String response = null;
            try {
                response = performTask(params);
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }

        @Override
        protected void onPostExecute(String response) {
            if (response == null) {
                Toast.makeText(getApplicationContext(), "Error! Login Unsuccessful", Toast.LENGTH_LONG).show();
                LoginManager.getInstance().logOut();

                sharedPreferences = getSharedPreferences(SharedPrefsFileName.tempFile, MODE_PRIVATE);
                sharedPreferences.edit().clear().commit();

                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
            }

            handleAuthResponse(response);
        }

        @Override
        protected String performTask(ArrayList<String>... params) {
            String response = null;
            try {
                // Setting the http connection to the server URL
                URL url = new URL(MyRESTAPIURLs.facebookAuthRegisterURL);
                HttpURLConnection httpURLConnection = HttpConnection.connectToServer(url, "POST");
                // data to be sent to the REST-API
                String name = params[0].get(0);
                String email = params[0].get(1);
                String accountType = params[0].get(2);
                String fcmToken = params[0].get(3);

                String postData = "name=" + URLEncoder.encode(name, "UTF-8") + "&email=" + URLEncoder.encode(email, "UTF-8")
                        + "&account_type=" + URLEncoder.encode(accountType, "UTF-8") +
                        "&gcm_registration_id=" + URLEncoder.encode(fcmToken, "UTF-8");

                httpURLConnection.setFixedLengthStreamingMode(postData.getBytes().length);
                // Sending data to server
                HttpConnection.sendToServer(httpURLConnection, postData);
                // Checking if response code from REST-API is 200
                if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    // Get response from server
                    InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                    // Processing response
                    response = HttpConnection.getResponse(inputStream);
                }
                httpURLConnection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return response;
        }
    }
}
