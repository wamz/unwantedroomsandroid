package wamz.the.unwantedroomsandroid.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import wamz.the.unwantedroomsandroid.JSONParser;
import wamz.the.unwantedroomsandroid.MyRESTAPIURLs;
import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.SharedPrefsFileName;
import wamz.the.unwantedroomsandroid.models.Bid;
import wamz.the.unwantedroomsandroid.models.Bids;
import wamz.the.unwantedroomsandroid.models.Hotel;
import wamz.the.unwantedroomsandroid.models.Hotels;
import wamz.the.unwantedroomsandroid.models.Image;
import wamz.the.unwantedroomsandroid.models.Images;
import wamz.the.unwantedroomsandroid.models.Room;
import wamz.the.unwantedroomsandroid.models.Rooms;
import wamz.the.unwantedroomsandroid.pickers.DatePickerFragment;

/**
 * Created by The on 07-Dec-15.
 */
public class DashBoardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DatePickerDialog.OnDateSetListener {
    private static final MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");
    private EditText destinationCityEditText;
    private Spinner roomSizeSpinner;
    private TextView checkInTextView, checkOutTextView;
    private Button searchButton;
    private String selectedRoomSize;
    private Hotels hotels;
    private Rooms rooms;
    private Images images;
    private Bids bids;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;
    private OkHttpClient okHttpClient;
    private SharedPreferences sharedPreferences;
    private View index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        setUpToolBar();
        setUpNavigationDrawer();
        setUpUIElements();
        displayRoomSizes();
        searchButtonClicked();

        sharedPreferences = this.getSharedPreferences(SharedPrefsFileName.tempFile, this.MODE_PRIVATE);
        Log.i("FB Name", String.valueOf(sharedPreferences.getString("Name", null)));

        selectCheckInDate();
        selectCheckOutDate();
    }

    private void setUpToolBar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setUpNavigationDrawer() {
        try {
            drawerLayout = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                    this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
            drawerLayout.addDrawerListener(toggle);
            toggle.syncState();

            navigationView = (NavigationView) findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    private void setUpUIElements() {
        destinationCityEditText = (EditText) findViewById(R.id.fragment_search_destination_city_edittext);
        roomSizeSpinner = (Spinner) findViewById(R.id.fragment_search_room_size_spinner);
        checkInTextView = (TextView) findViewById(R.id.activity_dashboard_check_in_textview);
        checkOutTextView = (TextView) findViewById(R.id.activity_dashboard_check_out_textview);
        searchButton = (Button) findViewById(R.id.fragment_search_button);
    }

    private boolean searchDataEntered() {
        return !destinationCityEditText.getText().equals("Which city are travelling to?") &&
                !selectedRoomSize.equals("Select Room Size");
    }

    private ArrayList<String> createListOfRoomSizes() {
        ArrayList<String> roomSizesList = new ArrayList<String>();
        roomSizesList.add("Select Room Size");
        roomSizesList.add("Single Room");
        roomSizesList.add("Double Room");

        return roomSizesList;
    }

    private void displayRoomSizes() {
        ArrayList<String> listOfRoomSizes = createListOfRoomSizes();

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                R.layout.my_dropdown_textbox, listOfRoomSizes);

        roomSizeSpinner.setAdapter(arrayAdapter);

        roomSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView) view;
                selectedRoomSize = textView.getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        Intent intent = null;
        switch (item.getItemId()) {
            case R.id.nav_profile:
                intent = new Intent(this, ProfileActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_dashboard:
                intent = new Intent(this, DashBoardActivity.class);
                startActivity(intent);
                break;
            case R.id.nav_bids:
                intent = new Intent(this, BidsActivity.class);
                startActivity(intent);
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.activity_drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void selectCheckInDate() {
        checkInTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = v;
                DialogFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getSupportFragmentManager(), "DatePicker");
            }
        });
    }

    private void selectCheckOutDate() {
        checkOutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index = v;
                DialogFragment datePickerFragment = new DatePickerFragment();
                datePickerFragment.show(getSupportFragmentManager(), "DatePicker");
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;

        if (index == checkInTextView)
            displaySelectedDate(checkInTextView, year, monthOfYear, dayOfMonth);

        if (index == checkOutTextView)
            displaySelectedDate(checkOutTextView, year, monthOfYear, dayOfMonth);

        Log.i("CheckInTv", checkInTextView.getText().toString());
        Log.i("CheckOutTv", checkOutTextView.getText().toString());
    }

    private void displaySelectedDate(TextView textView, int year, int month, int day) {
        if (month < 10 && day < 10)
            textView.setText(year + " - 0" + month + " - 0" + day);
        else if (month < 10)
            textView.setText(year + " - 0" + month + " - " + day);
        else if (day < 10)
            textView.setText(year + " - " + month + " - 0" + day);
        else
            textView.setText(year + " - " + month + " - " + day);
    }

    /* SEARCH FUNCTIONALITY */

    private ArrayList<String> searchParams() {
        ArrayList<String> params = new ArrayList<String>();

        if (!searchDataEntered()) {
            Toast.makeText(this, "All fields are required!", Toast.LENGTH_LONG).show();
            return null;
        }

        SharedPreferences sharedPreferences = this.getSharedPreferences(SharedPrefsFileName.tempFile, this.MODE_PRIVATE);
        String deviceKey = String.valueOf(sharedPreferences.getString("deviceKey", null));

        if (deviceKey == null)
            return null;

        params.add(deviceKey);
        params.add(destinationCityEditText.getText().toString());
        params.add(selectedRoomSize);

        return params;
    }

    private void searchForRooms(ArrayList<String> params) {
        try {
            String deviceKey = params.get(0);
            String townCity = params.get(1);
            String roomSize = params.get(2);

            String postParams = "town_city=" + URLEncoder.encode(townCity, "UTF-8") +
                    "&room_size=" + URLEncoder.encode(roomSize, "UTF-8");

            RequestBody body = RequestBody.create(mediaType, postParams);
            Request request = new Request.Builder()
                    .url(MyRESTAPIURLs.searchURL)
                    .addHeader("Authorization", deviceKey)
                    .post(body)
                    .build();

            okHttpClient = new OkHttpClient();

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        "No Hotels with Rooms to bid for were found in the city you searched for!",
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                        return;
                    }

                    parseSearchResults(response.body().string());
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseSearchResults(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray hotelsJSONArray = jsonObject.getJSONArray("Hotels");
            JSONArray roomsJSONArray = jsonObject.getJSONArray("Rooms");
            JSONArray imagesJSONArray = jsonObject.getJSONArray("Images");
            JSONArray bidsJSONArray = jsonObject.getJSONArray("Bids");

            ArrayList<Hotel> listOfHotels = JSONParser.parseHotel(hotelsJSONArray);
            hotels = JSONParser.parseHotels(listOfHotels);
            ArrayList<Image> listOfImages = JSONParser.parseImage(imagesJSONArray);
            images = JSONParser.parseImages(listOfImages);
            ArrayList<Bid> listOfBids = JSONParser.parseBid(bidsJSONArray);
            bids = JSONParser.parseBids(listOfBids);
            ArrayList<Room> listOfRooms = JSONParser.parseRoom(roomsJSONArray, hotels, images, bids);
            rooms = JSONParser.parseRooms(listOfRooms);

            Intent intent = new Intent(this, SearchedRoomsActivity.class);
            intent.putExtra("Rooms", rooms);
            startActivity(intent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void searchButtonClicked() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchParams() != null && searchParams().size() == 3)
                    searchForRooms(searchParams());
            }
        });
    }
}
