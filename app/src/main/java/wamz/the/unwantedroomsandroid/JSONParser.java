package wamz.the.unwantedroomsandroid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import wamz.the.unwantedroomsandroid.models.Bid;
import wamz.the.unwantedroomsandroid.models.Bids;
import wamz.the.unwantedroomsandroid.models.Hotel;
import wamz.the.unwantedroomsandroid.models.Hotels;
import wamz.the.unwantedroomsandroid.models.Image;
import wamz.the.unwantedroomsandroid.models.Images;
import wamz.the.unwantedroomsandroid.models.Room;
import wamz.the.unwantedroomsandroid.models.Rooms;

/**
 * Created by The on 10-Jan-16.
 */
public class JSONParser {

    public static Hotel parseHotel(JSONObject jsonObject) {
        Hotel hotel = null;
        try {
            hotel = new Hotel();

            if (jsonObject.has("id"))
                hotel.setId(Integer.parseInt(jsonObject.getString("id")));

            if (jsonObject.has("name"))
                hotel.setName(jsonObject.getString("name"));

            if (jsonObject.has("address_line_1"))
                hotel.setAddressLine1(jsonObject.getString("address_line_1"));

            if (jsonObject.has("address_line_2"))
                hotel.setAddressLine2(jsonObject.getString("address_line_2"));

            if (jsonObject.has("town_city"))
                hotel.setTownCity(jsonObject.getString("town_city"));

            if (jsonObject.has("county"))
                hotel.setCounty(jsonObject.getString("county"));

            if (jsonObject.has("postcode"))
                hotel.setPostcode(jsonObject.getString("postcode"));

            if (jsonObject.has("country"))
                hotel.setCountry(jsonObject.getString("country"));

            if (jsonObject.has("telephone_number"))
                hotel.setTelephoneNumber(jsonObject.getString("telephone_number"));

            if (jsonObject.has("email"))
                hotel.setEmail(jsonObject.getString("email"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return hotel;
    }

    public static ArrayList<Hotel> parseHotel(JSONArray jsonArray) {
        ArrayList<Hotel> listOfHotels = null;
        try {
            listOfHotels = new ArrayList<Hotel>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Hotel hotel = new Hotel();

                if (jsonObject.has("id"))
                    hotel.setId(Integer.parseInt(jsonObject.getString("id")));

                if (jsonObject.has("name"))
                    hotel.setName(jsonObject.getString("name"));

                if (jsonObject.has("address_line_1"))
                    hotel.setAddressLine1(jsonObject.getString("address_line_1"));

                if (jsonObject.has("address_line_2"))
                    hotel.setAddressLine2(jsonObject.getString("address_line_2"));

                if (jsonObject.has("town_city"))
                    hotel.setTownCity(jsonObject.getString("town_city"));

                if (jsonObject.has("county"))
                    hotel.setCounty(jsonObject.getString("county"));

                if (jsonObject.has("postcode"))
                    hotel.setPostcode(jsonObject.getString("postcode"));

                if (jsonObject.has("country"))
                    hotel.setCountry(jsonObject.getString("country"));

                if (jsonObject.has("telephone_number"))
                    hotel.setTelephoneNumber(jsonObject.getString("telephone_number"));

                if (jsonObject.has("email"))
                    hotel.setEmail(jsonObject.getString("email"));

                listOfHotels.add(hotel);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listOfHotels;
    }

    public static Hotels parseHotels(ArrayList<Hotel> listOfHotels) {
        Hotels hotels = new Hotels();
        hotels.setListOfHotels(listOfHotels);

        return hotels;
    }

    public static ArrayList<Image> parseImage(JSONArray jsonArray) {
        ArrayList<Image> listOfImages = null;
        String baseImageURL = "http://192.168.1.69:8000";
        try {
            listOfImages = new ArrayList<Image>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Image image = new Image();

                if (jsonObject.has("id"))
                    image.setId(Integer.parseInt(jsonObject.getString("id")));

                if (jsonObject.has("name"))
                    image.setName(jsonObject.getString("name"));

                if (jsonObject.has("ext"))
                    image.setExtension(jsonObject.getString("ext"));

                if (jsonObject.has("room"))
                    image.setRoomID(Integer.parseInt(jsonObject.getString("room")));

                if (jsonObject.has("room_image"))
                    image.setImageURL(baseImageURL + jsonObject.getString("room_image"));

                listOfImages.add(image);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listOfImages;
    }

    public static Images parseImages(ArrayList<Image> listOfImages) {
        Images images = new Images();
        images.setListOfImages(listOfImages);

        return images;
    }

    public static Room parseRoom(JSONObject jsonObject, Hotel hotel, Images images, Bid bid) {
        Room room = null;
        String baseImageURL = "http://192.168.1.69:8000";
        try {
            room = new Room();

            if (jsonObject.has("id"))
                room.setId(Integer.parseInt(jsonObject.getString("id")));

            if (jsonObject.has("room_number"))
                room.setRoomNumber(Integer.parseInt(jsonObject.getString("room_number")));

            if (jsonObject.has("room_name"))
                room.setRoomName(jsonObject.getString("room_name"));

            if (jsonObject.has("room_size"))
                room.setRoomSize(jsonObject.getString("room_size"));

            if (jsonObject.has("room_description"))
                room.setRoomDescription(jsonObject.getString("room_description"));

            if (jsonObject.has("currency"))
                room.setCurrency(jsonObject.getString("currency"));

            if (jsonObject.has("starting_bid_price"))
                room.setStartingBidPrice(Double.parseDouble(jsonObject.getString("starting_bid_price")));

            if (jsonObject.has("valid_for_bid_from"))
                room.setValidForBidFrom(jsonObject.getString("valid_for_bid_from"));

            if (jsonObject.has("valid_for_bid_until"))
                room.setValidForBidUntil(jsonObject.getString("valid_for_bid_until"));

            if (jsonObject.has("hotel"))
                room.setHotel(hotel);

            if (jsonObject.has("main_room_image"))
                room.setImageURL(baseImageURL + jsonObject.getString("main_room_image"));

            room.setRoomImages(images);

            if (jsonObject.has("leading_bid")) {
                if (jsonObject.isNull("leading_bid"))
                    room.setLeadingBid(0);
                else
                    room.setLeadingBid(Double.parseDouble(jsonObject.getString("leading_bid")));
            }

            room.setUserBid(bid);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return room;
    }

    public static ArrayList<Room> parseRoom(JSONArray jsonArray, Hotels hotels, Images images, Bids bids) {
        ArrayList<Room> listOfRooms = null;
        String baseImageURL = "http://192.168.1.69:8000";
        try {
            listOfRooms = new ArrayList<Room>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Room room = new Room();

                if (jsonObject.has("id"))
                    room.setId(Integer.parseInt(jsonObject.getString("id")));

                if (jsonObject.has("room_number"))
                    room.setRoomNumber(Integer.parseInt(jsonObject.getString("room_number")));

                if (jsonObject.has("room_name"))
                    room.setRoomName(jsonObject.getString("room_name"));

                if (jsonObject.has("room_size"))
                    room.setRoomSize(jsonObject.getString("room_size"));

                if (jsonObject.has("room_description"))
                    room.setRoomDescription(jsonObject.getString("room_description"));

                if (jsonObject.has("currency"))
                    room.setCurrency(jsonObject.getString("currency"));

                if (jsonObject.has("starting_bid_price"))
                    room.setStartingBidPrice(Double.parseDouble(jsonObject.getString("starting_bid_price")));

                if (jsonObject.has("valid_for_bid_from"))
                    room.setValidForBidFrom(jsonObject.getString("valid_for_bid_from"));

                if (jsonObject.has("valid_for_bid_until"))
                    room.setValidForBidUntil(jsonObject.getString("valid_for_bid_until"));

                if (jsonObject.has("hotel")) {
                    for (Hotel hotel : hotels.getListOfHotels()) {
                        if (Integer.parseInt(jsonObject.getString("hotel")) != hotel.getId())
                            continue;

                        room.setHotel(hotel);
                        break;
                    }
                }

                if (jsonObject.has("main_room_image"))
                    room.setImageURL(baseImageURL + jsonObject.getString("main_room_image"));

                room.setRoomImages(images);

                if (jsonObject.has("leading_bid")) {
                    if (jsonObject.isNull("leading_bid"))
                        room.setLeadingBid(0);
                    else
                        room.setLeadingBid(Double.parseDouble(jsonObject.getString("leading_bid")));
                }

                for (Bid bid : bids.getListOfBids()) {
                    if (bid.getRoomID() != Integer.parseInt(jsonObject.getString("id")))
                        continue;

                    room.setUserBid(bid);
                }

                listOfRooms.add(room);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listOfRooms;
    }

    public static Rooms parseRooms(ArrayList<Room> listOfRooms) {
        Rooms rooms = new Rooms();
        rooms.setListOfRooms(listOfRooms);

        return rooms;
    }

    public static ArrayList<Bid> parseBid(JSONArray jsonArray) {
        ArrayList<Bid> listOfBids = null;
        try {
            listOfBids = new ArrayList<Bid>();

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                Bid bid = new Bid();

                if (jsonObject.has("id"))
                    bid.setId(Integer.parseInt(jsonObject.getString("id")));

                if (jsonObject.has("room"))
                    bid.setRoomID(Integer.parseInt(jsonObject.getString("room")));

                if (jsonObject.has("app_user"))
                    bid.setUserID(Integer.parseInt(jsonObject.getString("app_user")));

                if (jsonObject.has("amount"))
                    bid.setAmount(Double.parseDouble(jsonObject.getString("amount")));

                listOfBids.add(bid);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listOfBids;
    }

    public static Bids parseBids(ArrayList<Bid> listOfBids) {
        Bids bids = new Bids();
        bids.setListOfBids(listOfBids);

        return bids;
    }

}
