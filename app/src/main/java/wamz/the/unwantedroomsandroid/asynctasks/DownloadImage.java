package wamz.the.unwantedroomsandroid.asynctasks;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;

import wamz.the.unwantedroomsandroid.R;

/**
 * Created by The on 06-Mar-16.
 */
public class DownloadImage extends AsyncTask<String, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;

    public DownloadImage(ImageView imageView) {
        imageViewReference = new WeakReference<ImageView>(imageView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        return downloadImageBitmap(params[0]);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        try {
            if (isCancelled())
                bitmap = null;

            if (imageViewReference == null)
                return;

            ImageView imageView = imageViewReference.get();

            if (imageView == null)
                return;

            if (bitmap == null)
                imageView.setImageResource(R.mipmap.ic_launcher);

            imageView.setImageBitmap(bitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Bitmap downloadImageBitmap(String url) {
        Bitmap imageBitmap = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL imageURL = new URL(url);
            httpURLConnection = (HttpURLConnection) imageURL.openConnection();

            if (httpURLConnection.getResponseCode() != HttpURLConnection.HTTP_OK)
                return null;

            InputStream inputStream = new BufferedInputStream(httpURLConnection.getInputStream());
            imageBitmap = BitmapFactory.decodeStream(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }

        return imageBitmap;
    }
}
