package wamz.the.unwantedroomsandroid;

/**
 * Created by The on 29-Dec-15.
 */
public class MyRESTAPIURLs
{
    // 10.0.2.2 is used so android emulator can access its localhost
    //public static String baseURL = "http://10.0.2.2:8080/projects/UnwantedRoomsRESTAPI/";

    // use this baseURL when running app using desktop or android device
    public static String baseURL = "http://192.168.1.69:8000/";
//    public static String baseURL = "http://172.16.155.166:8080/projects/UnwantedRoomsRESTAPI/";

    // AUTH URLs
    public static String facebookAuthLoginURL = baseURL+"fbAuthLogin/";
    public static String facebookAuthRegisterURL = baseURL+"fbAuthRegister/";
    public static String addGCMURL = baseURL+"addGCM";

    // USER URLs
    public static String userDetailsURL = baseURL+"user";
    public static String searchURL = baseURL+"search/";

    // BID URLs
    public static String roomsURL = baseURL+"rooms/";
    public static String bidsURL = baseURL+"bids/";
}
