package wamz.the.unwantedroomsandroid.baseclasses;

import android.os.AsyncTask;

import java.util.ArrayList;

/**
 * Created by WAMZ on 02/01/2016.
 */
public abstract class BaseAsyncTask extends AsyncTask<ArrayList<String>, Void, String>
{
    @Override
    protected abstract String doInBackground(ArrayList<String>... params);

    @Override
    protected abstract void onPostExecute(String response);

    protected abstract String performTask(ArrayList<String>... params);
}
