package wamz.the.unwantedroomsandroid.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.Utils;
import wamz.the.unwantedroomsandroid.asynctasks.DownloadImage;
import wamz.the.unwantedroomsandroid.models.Hotel;
import wamz.the.unwantedroomsandroid.models.Room;

/**
 * Created by The on 20-Jan-16.
 */
public class BidsAdapter extends ArrayAdapter<Room> {
    ArrayList<Room> listOfRooms;
    int listitem_layout;
    Context context;
    LayoutInflater layoutInflater;

    public BidsAdapter(Context context, int listitem_layout, ArrayList<Room> listOfRooms) {
        super(context, listitem_layout, listOfRooms);
        this.listOfRooms = listOfRooms;
        this.listitem_layout = listitem_layout;
        this.context = context;
        // inflating the layout of the activity_listview_listitem
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Using viewholder to recycle inflate layout views
        ViewHolder viewHolder;

        // Checking if view is null
        if (view != null)
            viewHolder = (ViewHolder) view.getTag(); // recycling the viewholder

        // inflating the activity_listview_listitem layout and assigning it to a view
        view = layoutInflater.inflate(listitem_layout, null);
        viewHolder = new ViewHolder();
        // Assigning the viewholder to the list item imageview and textview
        viewHolder.roomImageView = (ImageView) view.findViewById(R.id.listview_listitem_imageview);
        viewHolder.roomTextView = (TextView) view.findViewById(R.id.listview_listitem_room_name_textview);
        viewHolder.bidStatusTextView = (TextView) view.findViewById(R.id.listview_listitem_bid_status_textview);
        // setting a tag for the view so it can be recycled for reuse in the listview
        view.setTag(viewHolder);

        Room room = (Room) listOfRooms.get(position);

        if (room.getLeadingBid() != 0 && room.getUserBid() != null)
            viewHolder.roomTextView.setText(room.getRoomName() + " @ " + room.getHotel().getName() + "\n" +
                    "Leading Bid: " + Utils.GetCorrectCurrency(room) + Utils.TwoDecimalPlacesFormat(room.getLeadingBid()) + "\n" +
                    "Your Current Bid: " + Utils.GetCorrectCurrency(room) + Utils.TwoDecimalPlacesFormat(room.getUserBid().getAmount()));
        else
            viewHolder.roomTextView.setText(room.getRoomName() + " @ " + room.getHotel().getName() + "\n" +
                    "Leading Bid: " + Utils.GetCorrectCurrency(room) + Utils.TwoDecimalPlacesFormat(room.getLeadingBid()) + "\n" +
                    "Your Current Bid: " + Utils.GetCorrectCurrency(room) + Utils.TwoDecimalPlacesFormat(0.00));

        new DownloadImage(viewHolder.roomImageView).execute(room.getImageURL());

        return view;
    }

    static class ViewHolder {
        public ImageView roomImageView;
        public TextView roomTextView;
        public TextView bidStatusTextView;
    }
}
