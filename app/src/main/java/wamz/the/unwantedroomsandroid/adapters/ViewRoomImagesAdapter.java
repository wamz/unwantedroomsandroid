package wamz.the.unwantedroomsandroid.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import wamz.the.unwantedroomsandroid.R;
import wamz.the.unwantedroomsandroid.asynctasks.DownloadImage;

/**
 * Created by The on 20-Jan-16.
 */
public class ViewRoomImagesAdapter extends ArrayAdapter<String> {
    ArrayList<String> listOfURLs;
    int listitemLayout;
    Context context;
    LayoutInflater layoutInflater;

    public ViewRoomImagesAdapter(Context context, int listitemLayout, ArrayList<String> listOfURLs) {
        super(context, listitemLayout, listOfURLs);
        this.listOfURLs = listOfURLs;
        this.listitemLayout = listitemLayout;
        this.context = context;
        // inflating the layout of the activity_listview_listitem
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        // Using viewholder to recycle inflate layout views
        ViewHolder viewHolder;

        // Checking if view is null
        if (view != null)
            viewHolder = (ViewHolder) view.getTag(); // recycling the viewholder

        // inflating the activity_listview_listitem layout and assigning it to a view
        view = layoutInflater.inflate(listitemLayout, null);
        viewHolder = new ViewHolder();
        // Assigning the viewholder to the list item imageview and textview
        viewHolder.roomImageView = (ImageView) view.findViewById(R.id.view_room_images_listitem_imageview);
        // setting a tag for the view so it can be recycled for reuse in the listview
        view.setTag(viewHolder);

        String imageURL = (String) listOfURLs.get(position);

        new DownloadImage(viewHolder.roomImageView).execute(imageURL);

        return view;
    }

    static class ViewHolder {
        public ImageView roomImageView;
    }
}
