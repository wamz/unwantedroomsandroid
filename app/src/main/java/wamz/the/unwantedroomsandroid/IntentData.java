package wamz.the.unwantedroomsandroid;

import android.os.Bundle;

import com.facebook.Profile;

import wamz.the.unwantedroomsandroid.models.Bid;
import wamz.the.unwantedroomsandroid.models.Bids;
import wamz.the.unwantedroomsandroid.models.Hotel;
import wamz.the.unwantedroomsandroid.models.Hotels;
import wamz.the.unwantedroomsandroid.models.Images;
import wamz.the.unwantedroomsandroid.models.Room;
import wamz.the.unwantedroomsandroid.models.Rooms;

/**
 * Created by The on 23-Jan-16.
 */
public class IntentData
{
    public static Profile getProfileFromIntent(Bundle bundle) {
        return bundle.getParcelable("Profile");
    }

    public static Rooms getRoomsFromIntent(Bundle bundle) {
        return bundle.getParcelable("Rooms");
    }

    public static Room getRoomFromIntent(Bundle bundle) {
        return bundle.getParcelable("Room");
    }

    public static Hotels getHotelsFromIntent(Bundle bundle) {
        return bundle.getParcelable("Hotels");
    }

    public static Hotel getHotelFromIntent(Bundle bundle) {
        return bundle.getParcelable("Hotel");
    }

    public static Bids getBidsFromIntent(Bundle bundle) {
        return bundle.getParcelable("Bids");
    }

    public static Bids getMyBidsFromIntent(Bundle bundle) {
        return bundle.getParcelable("Bids");
    }

    public static Bid getBidFromIntent(Bundle bundle) {
        return bundle.getParcelable("Bid");
    }

    public static Images getImagesFromIntent(Bundle bundle) {
        return bundle.getParcelable("Images");
    }

    public static String getImageURL(Bundle bundle) {
        return bundle.getString("ImageURL");
    }
}
