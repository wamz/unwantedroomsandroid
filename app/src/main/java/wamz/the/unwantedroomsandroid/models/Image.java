package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by The on 03-Mar-16.
 */
public class Image implements Parcelable
{
    private int id;
    private String name;
    private String extension;
    private int roomID;
    private String imageURL;

    public Image(){
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(extension);
        out.writeInt(roomID);
        out.writeString(imageURL);
    }

    private Image(Parcel in){
        id = in.readInt();
        name = in.readString();
        extension = in.readString();
        roomID = in.readInt();
        imageURL = in.readString();
    }

    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>(){
        public Image createFromParcel(Parcel in){
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}
