package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by The on 13-Dec-15.
 */
public class Bids implements Parcelable
{
    private ArrayList<Bid> listOfBids;

    public Bids() {
    }

    public ArrayList<Bid> getListOfBids() {
        return listOfBids;
    }

    public void setListOfBids(ArrayList<Bid> listOfBids) {
        this.listOfBids = listOfBids;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeTypedList(listOfBids);
    }

    private Bids(Parcel in){
        listOfBids = new ArrayList<Bid>();
        in.readTypedList(listOfBids, Bid.CREATOR);
    }

    public static final Parcelable.Creator<Bids> CREATOR = new Parcelable.Creator<Bids>(){
        public Bids createFromParcel(Parcel in){
            return new Bids(in);
        }

        @Override
        public Bids[] newArray(int size) {
            return new Bids[size];
        }
    };
}
