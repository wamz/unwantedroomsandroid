package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by The on 13-Dec-15.
 */
public class Rooms implements Parcelable
{
    private ArrayList<Room> listOfRooms;

    public Rooms() {
    }

    public ArrayList<Room> getListOfRooms() {
        return listOfRooms;
    }

    public void setListOfRooms(ArrayList<Room> listOfRooms) {
        this.listOfRooms = listOfRooms;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeTypedList(listOfRooms);
    }

    private Rooms(Parcel in){
        listOfRooms = new ArrayList<Room>();
        in.readTypedList(listOfRooms, Room.CREATOR);
    }

    public static final Parcelable.Creator<Rooms> CREATOR = new Parcelable.Creator<Rooms>(){
        public Rooms createFromParcel(Parcel in){
            return new Rooms(in);
        }

        @Override
        public Rooms[] newArray(int size) {
            return new Rooms[size];
        }
    };
}
