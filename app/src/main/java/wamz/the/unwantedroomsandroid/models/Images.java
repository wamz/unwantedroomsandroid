package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by The on 03-Mar-16.
 */
public class Images implements Parcelable
{
    private ArrayList<Image> listOfImages;

    public Images(){
    }

    public ArrayList<Image> getListOfImages() {
        return listOfImages;
    }

    public void setListOfImages(ArrayList<Image> listOfImages) {
        this.listOfImages = listOfImages;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeTypedList(listOfImages);
    }

    private Images(Parcel in){
        listOfImages = new ArrayList<Image>();
        in.readTypedList(listOfImages, Image.CREATOR);
    }

    public static final Parcelable.Creator<Images> CREATOR = new Parcelable.Creator<Images>(){
        public Images createFromParcel(Parcel in){
            return new Images(in);
        }

        @Override
        public Images[] newArray(int size) {
            return new Images[size];
        }
    };
}
