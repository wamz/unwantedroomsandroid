package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by The on 13-Dec-15.
 */
public class Bid implements Parcelable
{
    private int id;
    private int roomID;
    private int userID;
    private double amount;

    public Bid() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(roomID);
        out.writeInt(userID);
        out.writeDouble(amount);
    }

    private Bid(Parcel in){
        id = in.readInt();
        roomID = in.readInt();
        userID = in.readInt();
        amount = in.readDouble();
    }

    public static final Parcelable.Creator<Bid> CREATOR = new Parcelable.Creator<Bid>(){
        public Bid createFromParcel(Parcel in){
            return new Bid(in);
        }

        @Override
        public Bid[] newArray(int size) {
            return new Bid[size];
        }
    };
}
