package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by The on 02-Jan-16.
 */
public class Hotels implements Parcelable
{
    private ArrayList<Hotel> listOfHotels;

    public Hotels(){
    }

    public ArrayList<Hotel> getListOfHotels() {
        return listOfHotels;
    }

    public void setListOfHotels(ArrayList<Hotel> listOfHotels) {
        this.listOfHotels = listOfHotels;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeTypedList(listOfHotels);
    }

    private Hotels(Parcel in){
        listOfHotels = new ArrayList<Hotel>();
        in.readTypedList(listOfHotels, Hotel.CREATOR);
    }

    public static final Parcelable.Creator<Hotels> CREATOR = new Parcelable.Creator<Hotels>(){
        public Hotels createFromParcel(Parcel in){
            return new Hotels(in);
        }

        @Override
        public Hotels[] newArray(int size) {
            return new Hotels[size];
        }
    };
}
