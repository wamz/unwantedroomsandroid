package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by The on 14-Dec-15.
 */
public class Hotel implements Parcelable
{
    private int id;
    private String name;
    private String addressLine1;
    private String addressLine2;
    private String townCity;
    private String county;
    private String postcode;
    private String country;
    private String telephoneNumber;
    private String email;

    public Hotel() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getTownCity() {
        return townCity;
    }

    public void setTownCity(String townCity) {
        this.townCity = townCity;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTelephoneNumber() {
        return telephoneNumber;
    }

    public void setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(addressLine1);
        out.writeString(addressLine2);
        out.writeString(townCity);
        out.writeString(county);
        out.writeString(postcode);
        out.writeString(country);
        out.writeString(telephoneNumber);
        out.writeString(email);
    }

    private Hotel(Parcel in){
        id = in.readInt();
        name = in.readString();
        addressLine1 = in.readString();
        addressLine2 = in.readString();
        townCity = in.readString();
        county = in.readString();
        postcode = in.readString();
        country = in.readString();
        telephoneNumber = in.readString();
        email = in.readString();
    }

    public static final Parcelable.Creator<Hotel> CREATOR = new Parcelable.Creator<Hotel>(){
        public Hotel createFromParcel(Parcel in){
            return new Hotel(in);
        }

        @Override
        public Hotel[] newArray(int size) {
            return new Hotel[size];
        }
    };
}
