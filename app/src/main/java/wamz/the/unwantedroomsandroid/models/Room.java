package wamz.the.unwantedroomsandroid.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by The on 13-Dec-15.
 */
public class Room implements Parcelable
{
    private int id;
    private int roomNumber;
    private String roomName;
    private String roomSize;
    private String roomDescription;
    private String currency;
    private double startingBidPrice;
    private String validForBidFrom;
    private String validForBidUntil;
    private String imageURL;
    private double leadingBid;
    private Hotel hotel;
    private Images roomImages;
    private Bid userBid;

    public Room() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomSize() {
        return roomSize;
    }

    public void setRoomSize(String roomSize) {
        this.roomSize = roomSize;
    }

    public String getRoomDescription() {
        return roomDescription;
    }

    public void setRoomDescription(String roomDescription) {
        this.roomDescription = roomDescription;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getStartingBidPrice() {
        return startingBidPrice;
    }

    public void setStartingBidPrice(double startingBidPrice) {
        this.startingBidPrice = startingBidPrice;
    }

    public String getValidForBidFrom() {
        return validForBidFrom;
    }

    public void setValidForBidFrom(String validForBidFrom) {
        this.validForBidFrom = validForBidFrom;
    }

    public String getValidForBidUntil() {
        return validForBidUntil;
    }

    public void setValidForBidUntil(String validForBidUntil) {
        this.validForBidUntil = validForBidUntil;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public double getLeadingBid() {
        return leadingBid;
    }

    public void setLeadingBid(double leadingBid) {
        this.leadingBid = leadingBid;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Images getRoomImages() {
        return roomImages;
    }

    public void setRoomImages(Images roomImages) {
        this.roomImages = roomImages;
    }

    public Bid getUserBid() {
        return userBid;
    }

    public void setUserBid(Bid userBid) {
        this.userBid = userBid;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(id);
        out.writeInt(roomNumber);
        out.writeString(roomName);
        out.writeString(roomSize);
        out.writeString(roomDescription);
        out.writeString(currency);
        out.writeDouble(startingBidPrice);
        out.writeString(validForBidFrom);
        out.writeString(validForBidUntil);
        out.writeString(imageURL);
        out.writeDouble(leadingBid);
        out.writeParcelable(hotel, flags);
        out.writeParcelable(roomImages, flags);
        out.writeParcelable(userBid, flags);
    }

    private Room(Parcel in){
        id = in.readInt();
        roomNumber = in.readInt();
        roomName = in.readString();
        roomSize = in.readString();
        roomDescription = in.readString();
        currency = in.readString();
        startingBidPrice = in.readDouble();
        validForBidFrom = in.readString();
        validForBidUntil = in.readString();
        imageURL = in.readString();
        leadingBid = in.readDouble();
        hotel = in.readParcelable(Hotel.class.getClassLoader());
        roomImages = in.readParcelable(Images.class.getClassLoader());
        userBid = in.readParcelable(Bid.class.getClassLoader());
    }

    public static final Parcelable.Creator<Room> CREATOR = new Parcelable.Creator<Room>(){
        public Room createFromParcel(Parcel in){
            return new Room(in);
        }

        @Override
        public Room[] newArray(int size) {
            return new Room[size];
        }
    };
}
