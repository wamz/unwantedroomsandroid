package wamz.the.unwantedroomsandroid;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by The on 29-Dec-15.
 */
public class HttpConnection
{
    public static HttpURLConnection connectToServer(URL url, String request){
        HttpURLConnection httpURLConnection = null;
        try {
            // Connecting to server url
            httpURLConnection = (HttpURLConnection) url.openConnection();

            if (request == "POST") {
                httpURLConnection.setDoOutput(true);
                httpURLConnection.setRequestMethod("POST");
                httpURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            }
            else if (request == "GET"){
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch");
            }
            else if(request == "DELETE"){
                httpURLConnection.setDoInput(true);
                httpURLConnection.setRequestMethod("DELETE");
                httpURLConnection.setRequestProperty("Accept-Encoding", "gzip, deflate, sdch");
            }
        }catch(Exception e){
            e.printStackTrace();
        }

        return httpURLConnection;
    }

    public static void sendToServer(HttpURLConnection httpURLConnection, String postParams){
        try {
            // Sending post data to server
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(httpURLConnection.getOutputStream());
            outputStreamWriter.write(postParams);
            outputStreamWriter.flush();
            outputStreamWriter.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String getResponse(InputStream inputStream){
        String response = null;
        try{
            // Processing response stream to extract the data
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String stream;
            // Iterating through the response stream
            while ((stream = bufferedReader.readLine()) != null) {
                // Extracting the data from the response stream and storing it in a stringBuilder
                stringBuilder.append(stream);
            }
            // Assigning the data stored in the stringBuilder to a string
            response = stringBuilder.toString();
        }catch (Exception e){
            e.printStackTrace();
        }

        return response;
    }
}
