package wamz.the.unwantedroomsandroid;

import wamz.the.unwantedroomsandroid.models.Room;

/**
 * Created by WAMZ on 03/04/2016.
 */
public class Utils {

    public static String GetCorrectCurrency(Room room) {
        String currency = null;
        switch (room.getCurrency()) {
            case "British Pound":
                currency = "£";
                break;
            case "US Dollar":
                currency = "$";
                break;
            case "Euro":
                currency = "\u20ac";
                break;
        }

        return currency;
    }

    public static String TwoDecimalPlacesFormat(double amount){
        return String.format("%.2f", amount);
    }
}
